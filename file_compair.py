'''
    when a file from Project Gutenberg was converted from utf-8 to gbk and then back again the flies were not the same
    size. This was writen to explore why. Turns out that Project Gutenberg uses CR+LF and python only uses LF.

    Note: this currently assumes an encoding which has 'CR' as 13
'''

import argparse


def parse_cli():
    parser = argparse.ArgumentParser()
    parser.add_argument("file1", help="first file to compair")
    parser.add_argument("file2", help="second file to compair")
    return parser.parse_args()




if __name__ == "__main__":
    cli_args = parse_cli()

    print(cli_args.file1)
    print(cli_args.file2)

    text1 = ''
    text2 = ''
    with open(cli_args.file1, 'rb') as file:
        text1 = file.read()

    with open(cli_args.file2, 'rb') as file:
        text2 = file.read()

    if text1 == text2:
        print('files are 100% the same')
    else:
        text1_13 = 0
        text2_13 = 0
        files_same = True
        for i in range(0, min(len(text1), len(text2))):
            if text1[i+text1_13] == 13:
                text1_13 += 1
            if text2[i+text2_13] == 13:
                text2_13 += 1
            if text1[i+text1_13] != text2[i+text2_13]:
                print(f'byte{i} {text1[i+text1_13]} | {text2[i+text2_13]}')
                files_same = False
                break
        if files_same:
            print(f"files are the same except for CR charactors file1 has {text1_13} file2 has {text2_13}")