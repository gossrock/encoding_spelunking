import argparse

def parse_cli():
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--mode", choices=['diff', 'base_none', 'all'], default="diff",
                        help="the mode of opperation 'diff'(default) finds characters that have diffrent encodings than the base, 'base_none' finds charactors where the base encoding of the same value is not a valid charactor, 'all' returns all charactors in the search encoding" )
    parser.add_argument("-e", "--output_encoding", help="the encoding to use during the file output", default="utf-8")
    parser.add_argument("-o", "--output_file", help="file to output to charactors to")
    parser.add_argument("-b", "--max_bytes", type=int, default=2)
    parser.add_argument("-p", "--print", type=bool, default=False)
    parser.add_argument("search_encoding", help="encoding to search")
    parser.add_argument("base_encoding", nargs='?', help="base encoding to compair to", default='utf-8')
    parser.set_defaults()
    return parser.parse_args()


def num_to_hex_for_bytearray(n):
	nhex = hex(n)
	nhexformated = nhex[2:]
	if len(nhexformated) % 2 == 1:
		nhexformated = '0'+ nhexformated
	return nhexformated

def encode(b:bytes, encoding:str) -> str:
    try:
        return str(b, encoding=encoding)
    except:
        return None

def write_file(filename, encoding, text):
    with open(filename, 'w', encoding=encoding) as file:
        file.write(text)

def encodings_search_for_difference(base_encoding, search_encoding, last, mode="diff"):
    found_list = []
    for n in range(0, last + 1):
        if n % 100000 == 0:
            print(f'\r{round(n/last * 100, 2)}%', end='')

        nbytes = bytes(bytearray.fromhex(num_to_hex_for_bytearray(n)))

        str_encode_base = encode(nbytes, encoding=base_encoding)
        str_encode_search = encode(nbytes, encoding=search_encoding)

        if str_encode_search != None and len(str_encode_search) == 1:
            if mode=='all':
                found_list.append(str_encode_search)
            elif str_encode_base != str_encode_search:
                if mode=="base_none" and str_encode_base == None:
                    found_list.append(str_encode_search)
                else:
                    found_list.append(str_encode_search)

    return found_list

if __name__ == '__main__':
    cli_args = parse_cli()
    byte = 2**8

    found_chars = encodings_search_for_difference(cli_args.base_encoding, cli_args.search_encoding, byte ** cli_args.max_bytes, mode=cli_args.mode)
    print()

    if cli_args.output_file is not None:
        write_file(cli_args.output_file, cli_args.output_encoding, ''.join(found_chars))

    if cli_args.print:
        print(''.join(found_chars))

    print(len(found_chars))







