import argparse

def parse_cli():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input_encoding", help="encoding of input file")
    parser.add_argument("-o", "--output_encoding", help="encoding of output file")
    parser.add_argument("input_file", help="file to convert")
    parser.add_argument("output_file", help="file to save convertion to")
    return parser.parse_args()

def file_text(filename, encoding):
    with open(filename, 'r', encoding=encoding) as file:
        text = file.read()
    return text

def write_file(filename, encoding, text):
    with open(filename, 'w', encoding=encoding) as file:
        file.write(text)

if __name__ == "__main__":
    cli_args = parse_cli()
    print(cli_args.input_encoding)
    print(cli_args.input_file)

    print(cli_args.output_encoding)
    print(cli_args.output_file)

    text = file_text(cli_args.input_file, cli_args.input_encoding)
    write_file(cli_args.output_file, cli_args.output_encoding, text)